---
title: Import Pre-Made Kali VirtualBox VM
description:
icon:
weight: 211
author: ["gamb1t",]
---

Importing the [Kali VirtualBox image](https://www.kali.org/get-kali/#kali-virtual-machines) is a four step after we have it downloaded.

We first launch VirtualBox:

![](import-vbox-1.png)

From here we will be wanting to select 'Import':

![](import-vbox-2.png)

We select the file we downloaded earlier and then we are able to continue forward:

![](import-vbox-3.png)

We can verify the settings that will be set here, and if we need to change any we can. Once we are happy we can select 'Import':

![](import-vbox-4.png)

We now just need to agree to the license agreement, and then we are all done. From here we can boot the VM and use it as normal. Remember the default login is kali for the user and kali for the password!